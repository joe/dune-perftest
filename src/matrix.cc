// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>

using namespace Dune;

template<class K, int n> 
void measureEigenValues()
{
  FieldMatrix<K, n, n> matrix;
  
  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      matrix[i][j] = (K)rand() / RAND_MAX;
    }
  }
  
  FieldVector<K, n> values;
  
  for (int i = 0; i < 300; ++i)
  {
    FMatrixHelp::eigenValues(matrix, values);
  }
}

int main()
{
  try {
    measureEigenValues<double, 300>();
  }
  catch (Dune::Exception & e)
  {
    std::cerr << "Exception: " << e << std::endl;
  }
}
