from database import *
from visualize import *

import os

##
# \brief Handles the generation of HTML from measurement data
#
# This class contains convenience functions for generating
# a large number of HTML files from a set of tests.
# It calls the methods in perftest.visualize.templates, but also remembers
# shared settings, outputs modes and test data.
#
class HtmlVisualization:
  ##
  # Constructs a new visualization
  #
  # \param base_name the directory where the output HTML files should be generated
  # \param tests a list of test names
  # \param modes a dict of outputs modes, where the key is a machine-readable name, and the value is the human-readable name
  # \param db_file path to the database file with results
  def __init__(self, base_name, tests, modes, db_file):
    self.base_name = base_name
    self.modes = modes
    self.tests = {t : t for t in tests}
    self.db_file = db_file

  ##
  # Returns the HTML code for navigating between tests
  # \param test the currently active test
  # \param mode the currently active output mode
  def get_mode_navigation(self, test, mode):
    base = os.path.join(self.base_name, test)
    ret = ''
    t = load_template('navigation_item')

    # First add Overview, Compile and Run modes, in this order
    for i in ['cr', 'compile', 'run']:
      if i in self.modes:
        context = {
          'class' : 'active' if i == mode else '',
          'destination' : base + '_' + i + '.html',
          'name' : self.modes[i],
        }
        ret += t.substitute(context)
        ret += '\n'

    # If there are any modes other then compile and run, add them here
    for m, n in self.modes.items():
      if not m in ['cr', 'compile', 'run']:
        context = {
          'class' : 'active' if m == mode else '',
          'destination' : base + '_' + m + '.html',
          'name' : n,
        }
        ret += t.substitute(context)
        ret += '\n'

    return ret

  ##
  # Returns the HTML code for navigating between modes
  # \param test the currently active test
  # \param mode the currently active output mode
  def get_test_navigation(self, test, mode):
    ret = ''
    t = load_template('navigation_item')

    ret += t.substitute({
      'class' : 'active' if test == '' else '',
      'destination' : os.path.join(self.base_name, 'index.html'),
      'name' : 'Summary',
    })

    for i, n in self.tests.items():
      context = {
        'class' : 'active' if i == test else '',
        'destination' : os.path.join(self.base_name, i + '_' + mode + '.html'),
        'name' : n,
      }
      ret += t.substitute(context)
      ret += '\n'

    return ret

  ##
  # Generates all test-specific output
  #
  # This function creates a HTML file for each combination of test and mode.
  # The resulting filenames have the form \a [base_name]/[test_dir]/[test]_[mode].html
  def generate_all(self):
    for test in self.tests:
      base_dir = os.path.join(self.base_name, os.path.dirname(test))
      import distutils.dir_util
      distutils.dir_util.mkpath(base_dir)
      base = os.path.join(self.base_name, test)
      for mode in self.modes:
        mode_nav = self.get_mode_navigation(test, mode)
        test_nav = self.get_test_navigation(test, mode)

        if mode != 'cr':
          visualize(test, mode, get_results(self.db_file, test, mode=mode), base_name=base,
                    mode_nav=mode_nav, test_nav=test_nav)
        else:
          visualize_compile_run(test, get_results(self.db_file, test), base_name=base,
                    mode_nav=mode_nav, test_nav=test_nav)

  ##
  # Generates a summary view with a table of the latest test times
  #
  # The resulting filename is \a [base_name]/index.html
  def generate_summary(self):
    table = ''
    modes = list(self.modes.keys())
    main_mode = modes[0]
    if 'cr' in modes and 'compile' in modes and 'run' in modes:
      modes.remove('cr')
      modes.remove('compile')
      modes.remove('run')
      modes = ['compile', 'run'] + modes
      main_mode = 'cr'

    header = '<thead><tr><th>Test name</th>'
    for mode in modes:
      header += "<th>" + self.modes[mode] + " duration" + "</th>"
    header += "</tr></thead>"

    table += header
    table += "<tbody>"

    for test in self.tests:
      res = get_last_results(self.db_file, test, modes)
      row = '<tr><td><a href="%s">%s</td>' % (os.path.join(self.base_name, test + "_" + main_mode + ".html"), test)
      for i in range(len(modes)):
        row += '<td><a href="%s">%s</td>' % (os.path.join(self.base_name, test + '_' + modes[i] + ".html"), format_duration(res[i]['duration']))
      row += "</tr>"
      table += row

    table += "</tbody>"

    visualize_summary(table, self.base_name, self.get_test_navigation('', main_mode))
