import os
import base64

try:
  import urllib.request
  import urllib.parse
except ImportError:
  import urllib
  
AUTH_FILE = 'perftest-auth.conf'

##
# Uploads \p filename to a server at \p destination
#
def upload_file(filename, destination, run_name, base_dir):
  with open(filename, 'rb') as f:
    body = f.read()
  
  name = os.path.basename(filename)
  data = {
    'run' : run_name,
    'name' : name,
    'logfile' : body,
  }
  
  username = ''
  password = ''
  abspath = os.path.join(base_dir, AUTH_FILE)
  try:
    with open(abspath, 'r') as auth:
      for line in auth:
        if line.startswith('username = '):
          username = line.split('=')[1].strip()
        elif line.startswith('password = '):
          password = line.split('=')[1].strip()
        elif line.startswith('server = ') and not destination:
          destination = line.split('=')[1].strip()
  except IOError:
    pass

  if not username or not password or not destination:        
    print("Warning: Not uploading data -- if you want to upload, create a file")
    print("named '%s'" % abspath) 
    print("with the following contents:")
    print("")
    print("username = <your username>")
    print("password = <your password>")
    print("server = http://<server url>/cgi-bin/receive.py")
    print("")
    return

  if hasattr(urllib, 'urlopen'):
    request = urllib.Request(destination, urllib.urlencode(data))
    base64string = base64.b64encode('%s:%s' % (username, password)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)
    u = urllib.urlopen(request)
    response = u.read()
  else:
    request = urllib.request.Request(destination, bytearray(urllib.parse.urlencode(data), 'utf-8'))
    base64string = base64.b64encode(bytearray('%s:%s' % (username, password), 'utf-8')).decode('utf-8')
    request.add_header("Authorization", "Basic %s" % base64string)
    u = urllib.request.urlopen(request)
    response = u.read().decode('utf-8')
    
  print(response)

if __name__ == "__main__":
  import sys
  upload_file(sys.argv[1], sys.argv[2])