from string import Template
from datetime import datetime

try:
  from visualize.statistics import *
except ImportError:
  pass

import os

##
# Loads a template named \p name
#
# Templates are loaded from the /template directory
# and named <\p name>.html
#
# \return a template object
def load_template(name):
  with open(os.path.join(os.path.dirname(__file__), 'templates', name + '.html')) as f:
    template_str = f.read()

  return Template(template_str)

##
# Displays a duration in a human-readable format
# \param seconds the duration in seconds
def format_duration(seconds):
  # TODO: Consider also the cases where the duration
  # is a different order of magnitude
  return "%.2f s" % seconds

## \cond
SYMBOLS = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
PREFIX = {}

for i, s in enumerate(SYMBOLS):
  PREFIX[s] = 1 << (i+1)*10

## \endcond

##
# Formats a number of bytes into a human-readable format
# \param n the number of bytes
def format_bytes(n):
  for s in reversed(SYMBOLS):
    if n >= PREFIX[s]:
      value = float(n) / PREFIX[s]
      return '%.1f %sB' % (value, s)

##
# Populates a HTML table row with data in \p result.
#
# This functions uses format_duration() to display durations and format_bytes() for memory and disk sizes.
# \param result the result of one measurement
# \param suffix an optional suffix if you wish to load a different HTML template
# \return a formatted HTML table row as a string
def table_row(result, suffix = None):
  if suffix:
    t = load_template('table_row_' + suffix)
  else:
    t = load_template('table_row')
  context = {
    'program_name' : result['program_name'],
    'command' : result['command'],
    'command_type' : result['command_type'],
    'compiler' : result['compiler'],
    'compiler_flags' : result['compiler_flags'],
    'start_time' : datetime.fromtimestamp(result['start_time']).isoformat(' '),
    'computer' : result['hostname'],
    'duration' : format_duration(result['duration']),
    'memory' : format_bytes(int(result['memory_rss_max'])),
    'revision' : result['revision'],
  }

  context['row_class'] = ''
  if 'outlier' in result and result['outlier']:
    if result['outlier'] == OUTLIER_VERY_LOW:
      context['row_class'] = 'success'
    elif result['outlier'] == OUTLIER_VERY_HIGH:
      context['row_class'] = 'error'
    elif result['outlier'] == OUTLIER_LOW:
      context['row_class'] = 'info'
    elif result['outlier'] == OUTLIER_HIGH:
      context['row_class'] = 'warning'

  return t.substitute(context)

##
# Creates an overview HTML file with results for both compile-time and run-time testing
#
# \param title the output page title
# \param results a list of performance measurement results
# \param base_name file prefix for all generated files
# \param mode_nav HTML code for navigation between modes
# \param test_nav HTML code for navigation between tests
def visualize_compile_run(title, results, base_name, mode_nav, test_nav):
  compile = 'Time, Duration, Memory\n'
  run = 'Time, Duration, Memory\n'

  compile_results = [result for result in results if result['command_type'] == 'compile']
  run_results = [result for result in results if result['command_type'] == 'run']

  try:
    compile_outliers = find_outliers([r['duration'] for r in compile_results], 1)
    compile_results = [dict(outlier=o, **r) for o, r in zip(compile_outliers, compile_results)]

    run_outliers = find_outliers([r['duration'] for r in run_results], 1)
    run_results = [dict(outlier=o, **r) for o, r in zip(run_outliers, run_results)]
  except NameError:
    print("Statistics not available -- please install NumPy to identify outliers")

  results = sorted(compile_results + run_results, key=lambda r: r['start_time'])

  for result in results:
    if result['command_type'] == 'compile':
      compile += '%s, %f, %f\n' % (datetime.fromtimestamp(result['start_time']).isoformat(), result['duration'], result['memory_rss_max'])
    elif result['command_type'] == 'run':
      run += '%s, %f, %f\n' % (datetime.fromtimestamp(result['start_time']).isoformat(), result['duration'], result['memory_rss_max'])

  t = load_template('results_compile_run')
  context = {
    'title' : title,
    'mode_nav' : mode_nav,
    'test_nav' : test_nav,
    'results_table' : '\n'.join([table_row(result, 'cr') for result in results]),
    'directory' : os.path.dirname(__file__),
    'csv_file_compile' : compile.replace('\n', '\\n" + \n"'),
    'csv_file_run' : run.replace('\n', '\\n" + \n"'),
  }

  with open(base_name + '_cr.html', 'w') as html:
    html.write(t.substitute(context))

##
# Creates a HTML files with \p results
#
# \param title the output page title
# \param mode the current test mode, such as \c 'run' or \c 'compile'
# \param results a list of performance measurement results
# \param base_name file prefix for all generated files
# \param mode_nav HTML code for navigation between modes
# \param test_nav HTML code for navigation between tests
def visualize(title, mode, results, base_name, mode_nav, test_nav):
  data = 'Time, Duration, Memory\n'

  try:
    outliers = find_outliers([r['duration'] for r in results], 1)
    results = [dict(outlier=o, **r) for o, r in zip(outliers, results)]
  except NameError:
    print("Statistics not available -- please install NumPy to identify outliers")

  for result in results:
    data += '%s, %f, %f\n' % (datetime.fromtimestamp(result['start_time']).isoformat(), result['duration'], result['memory_rss_max'])

  t = load_template('results')
  context = {
    'title' : title,
    'mode_nav' : mode_nav,
    'test_nav' : test_nav,
    'mode' : mode,
    'results_table' : '\n'.join([table_row(result) for result in results]),
    'directory' : os.path.dirname(__file__),
    'data' : data.replace('\n', '\\n" + \n"'),
  }

  with open(base_name + '_' + mode + '.html', 'w') as html:
    html.write(t.substitute(context))

def visualize_summary(table, base_name, test_nav):
  t = load_template('summary')
  context = {
    'results_table' : table,
    'test_nav' : test_nav,
    'directory' : os.path.dirname(__file__),
  }

  with open(os.path.join(base_name, 'index.html'), 'w') as html:
    html.write(t.substitute(context))
