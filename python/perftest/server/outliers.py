#!/usr/bin/env python

import cgi
import cgitb
import os
import math

from numpy import array

import psycopg2

from conf import DATABASE_OPTIONS, ASSETS_DIRECTORY
from common import *

cgitb.enable()

print("Content-Type: text/html")
print()

sigmas = 3

def find_outliers(values):
  a = array(values)
  mean = a.mean()
  dev = a.std() * sigmas
  
  idx = []
  for i in range(len(values)):
    if a[i] > mean + dev:
      idx.append(i)
      
  return idx

def check_test_computer(test, computer, mode, cursor):
  cursor.execute("SELECT duration FROM measurement WHERE program_name = %s AND hostname = %s and command_type = %s", (test, computer, mode))
  outliers = find_outliers([row[0] for row in cursor])
  
  if outliers:
    return 'Outliers: <span class="label label-important">%d</span>' % len(outliers)
  
  else:
    return '<span class="label label-success">No outliers</span>'
  
def check_test(test, cursor):
  cursor.execute("SELECT DISTINCT hostname FROM measurement WHERE program_name = %s", (test,))
  computers = [row[0] for row in cursor]
  
  cursor.execute("SELECT DISTINCT command_type FROM measurement WHERE program_name = %s", (test,))
  modes = [row[0] for row in cursor]
  
  table = '<h3>%s</h3>' % test
  table += '<dl class="dl-horizontal">'
  for c in computers:
    table += '<dt>%s</dt>' % c
    table += '<dd>'
    for m in modes:
      table += '<p>' + get_mode_name(m) + ': ' + check_test_computer(test, c, m, cursor) + '</p>'
    table += '</dd>'
  table += '</dl>'
  
  return table
  
def check_all():
  con = psycopg2.connect(DATABASE_OPTIONS)
  cursor = con.cursor()
  

  t = load_template('outliers_table')
  print(t.substitute({
    'results_table' : '\n'.join([check_test(test, cursor) for test in get_all_tests(cursor)]),
    'directory' : ASSETS_DIRECTORY,
    'base_url' : create_url('visualize'),
    'current_url' : rewrite_query(),
    'sigmas' : round(sigmas) if (abs(round(sigmas) - sigmas) < 0.1) else sigmas,
    'test_nav' : get_test_navigation('', '', get_all_tests(cursor)),
    'main_nav' : get_main_navigation('outliers'),
  }))

  cursor.close()
  con.close()


form = cgi.FieldStorage()

if 'sigma' in form:
  try:
    sigmas = float(form.getvalue('sigma'))
  except ValueError:
    sigmas = 3
  
if 'test' in form:
  print(check_test(form.getvalue('test')))
else:
  check_all()

