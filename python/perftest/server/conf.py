import os

##
# @package server.conf
#
# Configuration values that depend on the server and should be set individually. 

##
# Location for saving uploaded files. 
#
SPOOL_DIRECTORY = "/var/cache/lighttpd/uploads"

##
# Returns the directory for storing uploading files. 
# 
# If the directory does not exist yet, this function creates it. 
def get_spool_dir():
  if not os.path.exists(SPOOL_DIRECTORY):
    os.makedirs(SPOOL_DIRECTORY)
  return SPOOL_DIRECTORY

##
# Database option string, passed to psycopg2.connect()
#
DATABASE_OPTIONS = "dbname=dune-perftest user=miha"

##
# Directory with Bootstrap, Dygraphs and other assets
#
ASSETS_DIRECTORY = "/cgi-bin/assets"

##
# An array of users allowed to upload files to the server
# It is a list of tuples, where the first element in each tuple
# is the username, and the second is the password. 
#
# Passwords are stored in plain text, so this may not be the best security. 
# For high security, keep this empty and use SSH uploading. 
#
USERS = [
#  ('miha-test', 'test-pw'),
]

##
# Checks if a user with \p username and \p password is registered in the USERS array. 
# Returns \c True if such a user exists and \c False otherwise. 
#
def check_user(username, password):
  return (username, password) in USERS