import os
import string
import conf

try:
  from urllib.parse import urlencode, urlparse, urlunparse, parse_qsl
except ImportError:
  from urllib import urlencode
  from urlparse import urlparse, urlunparse, parse_qsl

##
# Dict of human-readable mode names. 
#
# Currently, only 'run' and 'compile' are supported, along
# with an empty mode called 'Overview'. 
#
MODE_NAMES = {
  'compile' : 'Compile',
  'run' : 'Run',
  '' : 'Overview',
}

##
# Returns a human-readable mode name for \p mode
#
# \sa MODE_NAMES
#
def get_mode_name(mode):
  return MODE_NAMES[mode] if mode in MODE_NAMES else mode

##
# Returns the current URL
#
# \param **kwargs additional query parameters may be specified as keyword arguments
#
def get_url(**kwargs):
  parts = urlparse('http://' + os.environ['SERVER_NAME'] + os.environ['REQUEST_URI'])
  query = dict(parse_qsl(parts[4]))

  query.update(kwargs)
  parts = [p for p in parts]
  parts[4] = urlencode(query)
  
  return urlunparse(parts)

##
# Returns the current URL with a new query string
#
# \param **kwargs query parameters may be specified as keyword arguments
#
def rewrite_query(**kwargs):
  parts = urlparse('http://' + os.environ['SERVER_NAME'] + os.environ['REQUEST_URI'])
  parts = [p for p in parts]
  parts[4] = urlencode(kwargs)
  
  return urlunparse(parts)

##
# Returns the URL of \p page with a new query string
#
# \param page the name of the page, such as 'visualize' or 'outliers'
# \param **kwargs query parameters may be specified as keyword arguments
#
def create_url(page, **kwargs):
  url = 'http://' + os.environ['SERVER_NAME'] + '/cgi-bin/%s.py' % page
  if kwargs:
    parts = [p for p in urlparse(url)]
    parts[4] = urlencode(kwargs)
    url = urlunparse(parts)
  return url

##
# Loads template \p name from the \c templates directory
#
# This function automatically adds the directory name and '.html' suffix
#
# \param name template name
# \return a string.Template object
#
def load_template(name):
  with open(os.path.join(os.path.dirname(__file__), 'templates', name + '.html')) as f:
    template_str = f.read()

  return string.Template(template_str)

##
# Displays a duration in a human-readable format
# \param seconds the duration in seconds
def format_duration(seconds):
  return "%.2f s" % seconds

SYMBOLS = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
PREFIX = {}

for i, s in enumerate(SYMBOLS):
  PREFIX[s] = 1 << (i+1)*10
  
##
# Formats a number of bytes into a human-readable format
# \param n the number of bytes
def format_bytes(n):
  for s in reversed(SYMBOLS):
    if n >= PREFIX[s]:
      value = float(n) / PREFIX[s]
      return '%.1f %sB' % (value, s)
    

##
# Returns the list of all modes (command types) for a given \p test
#
# \param cursor used to load data from the DB
# \param test the test name
#
def get_all_modes(cursor, test):
  cursor.execute("SELECT DISTINCT command_type FROM measurement WHERE program_name = %s;", (test,))
  return [''] + [v[0] for v in cursor]

##
# Returns the list of all tests (program names)
#
# \param cursor used to load data from the DB
#
def get_all_tests(cursor):
  cursor.execute("SELECT DISTINCT program_name FROM measurement ORDER BY program_name;")
  return [v[0] for v in cursor]

##
# Returns the list items for navigation between tests
# 
# \param test the currently active test
# \param mode the currently active mode
# \param tests all tests for which navigation items should be created
#
def get_test_navigation(test, mode, tests):
  ret = ''
  t = load_template('navigation_item')
  
  for n in tests:
    context = {
      'class' : 'active' if n == test else '',
      'destination' : create_url('visualize', test=n, mode=mode),
      'name' : n,
    }
    ret += t.substitute(context)
    ret += '\n'

  return ret

##
# Returns the list items for top-level navigation
# 
# \param page the currently active page
#
def get_main_navigation(page):
  ret = ''
  t = load_template('navigation_item')

  ret += t.substitute({
    'class' : 'active' if page == 'summary' else '',
    'destination' : create_url('visualize'),
    'name' : 'Summary',
  })
  ret += t.substitute({
    'class' : 'active' if page == 'outliers' else '',
    'destination' : create_url('outliers'),
    'name' : 'Outliers',
  })
  return ret

##
# Returns the list items for navigation between modes
# 
# \param test the currently active test
# \param mode the currently active mode
# \param modes all modes for which navigation items should be created
#
def get_mode_navigation(test, mode, modes):
  ret = ''
  t = load_template('navigation_item')

  # First add Overview, Compile and Run modes, in this order
  for i in ['', 'compile', 'run']:
    if i in modes:
      context = {
        'class' : 'active' if i == mode else '',
        'destination' : rewrite_query(test=test, mode=i),
        'name' : get_mode_name(i),
      }
      ret += t.substitute(context)
      ret += '\n'

  # If there are any modes other then compile and run, add them here
  for m in modes:
    if not m in ['', 'compile', 'run']:
      context = {
        'class' : 'active' if m == mode else '',
        'destination' : rewrite_query(test=test, mode=m),
        'name' : get_mode_name(m),
      }
      ret += t.substitute(context)
      ret += '\n'

  return ret
