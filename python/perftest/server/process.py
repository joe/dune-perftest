#!/usr/bin/env python

import cgi
import cgitb
import os
import shutil

import psycopg2

from conf import get_spool_dir, DATABASE_OPTIONS

cgitb.enable()

print("Content-Type: text/html")
print()
print("Start processing files in %s" % get_spool_dir())

##
# \brief Reads the log file \p logfile and returns its contents as a dict
#
# \param logfile the location of the log file with the test results
# \return the contents of the log file
def read_log_file(logfile):
  data = dict()
  with open(logfile) as f:
    for line in f.readlines():
      pair = line.split(':')
      if len(pair) == 2:
        key = pair[0].lower()
        value = pair[1].strip()
        data[key] = value
  return data

##
# Forms a PostgreSQL statement from a dict of values.
#
# This statement only contains column names and placeholders,
# values have to be passed separately to prevent SQL injection vulnerabilities.
#
def create_sql_statement(data):
  keys = ', '.join(data.keys())
  placeholders = ', '.join(['%%(%s)s' % key for key in data.keys()])
  statement = "INSERT INTO measurement (%s) VALUES (%s)" % (keys, placeholders)
  return statement

##
# Creates the \c measurement table if needed.
#
# If the table already exists, this function does nothing.
# \param con a connection to the database.
def create_tables(con):
  has_measurement_table = False
  cursor = con.cursor()

  cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'")
  for row in cursor.fetchall():
    if row[0] == 'measurement':
      has_measurement_table = True
      break

  if not has_measurement_table:
    cursor.execute('''CREATE TABLE measurement (
    _id SERIAL PRIMARY KEY,

    hostname TEXT,
    architecture TEXT,
    system TEXT,
    num_cpus INTEGER,
    processor TEXT,
    ram BIGINT,

    test_run TEXT,
    program_name TEXT,
    revision TEXT,
    command TEXT,
    command_type TEXT,
    start_time BIGINT,
    problem_size INTEGER,
    compiler TEXT,
    compiler_flags TEXT,

    duration REAL,
    returncode INTEGER,
    memory_rss_max REAL,
    memory_rss_avg REAL,
    cpu_percent_max REAL,
    cpu_percent_avg REAL)
    ''')
    con.commit()
  cursor.close()

##
# Returns \c True if \p data is a valid measurement result and \c False otherwise.
def is_valid_measurement(data):
  return 'duration' in data and data['duration']

spool = get_spool_dir()
con = psycopg2.connect(DATABASE_OPTIONS)
create_tables(con)
cursor = con.cursor()

for run in os.listdir(spool):
  dir = os.path.join(spool, run)
  for log in os.listdir(dir):
    logfile = os.path.join(dir, log)
    print("Processing log file %s ..." % logfile)

    data = read_log_file(logfile)
    if not 'test_run' in data:
      data['test_run'] = run
    if is_valid_measurement(data):
      try:
        statement = create_sql_statement(data)
        cursor.execute(statement, data)
        con.commit()
        os.remove(logfile)
      except psycopg2.ProgrammingError as e:
        print("Error in log file: ", e)
        con.rollback()
    else:
      print("Invalid measurement log file: " + logfile)

  print("Removing processed directory %s ..." % os.path.join(spool, run))
  shutil.rmtree(os.path.join(spool, run))

cursor.close()
con.close()

print("Processing finished")
